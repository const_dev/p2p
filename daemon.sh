#!/bin/bash

cd "$( dirname "$0" )"

if [ ! -d log ]; then
    mkdir log
fi

if [ ! -d py2 ]; then
    git clone git://github.com/pypa/virtualenv.git
    python2.7 virtualenv/virtualenv.py py2
    source py2/bin/activate
    pip install python-daemon
else
    source py2/bin/activate
fi

python gen_node_daemon.py $1
