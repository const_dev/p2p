\documentclass[letter,10pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage[left=3cm,right=3cm,top=3cm,bottom=3cm]{geometry}
\usepackage{array}
\usepackage{listings}
\usepackage{subfig}
\usepackage{url}
\usepackage{fancyvrb}[9pt]

\parindent=0pt
\parskip=.6em

\DefineVerbatimEnvironment{shell}{Verbatim}
{%fontsize=\small,
 boxwidth=3in,frame=single,framerule=0.4pt,framesep=5pt,
 %xleftmargin=3em, xrightmargin=3em
}

%\usepackage{appendix}
%
\newenvironment{proof}{{\bf Proof. }}{\hfill $\square$\medskip}
\newenvironment{claim}{\paragraph{{\bf Claim. }}}{\hfill \\}
\newcommand{\world}{{\bf World }}

\title{CS677 Distributed Operating Systems - Lab 3\\Program Design}
\author{Fabricio Ferreira and Steve Cheng-Xian Li}
\date{Due : April 30 2013}

\begin{document}

\maketitle

\section{Overall program design}

Our program is mostly based on the one from the previous lab, but
now multiple bird launches are allowed in the same ``round'', two leaders
share coordination tasks, a database stores pigs statuses after each launch,
and one coordinator can fail after what the other takes over. In addition, we
implemented the extra credit features, namely: monitoring the load on the server
and dynamically replicating the (single) coordinator if needed.

This version also features some other improvements, such as
implementing termination detection to decide when to query the pigs (as opposed
to waiting for arbitrary amounts of time), replacing the python-graph module
by the json module in order to create and represent the network topologies,
providing a daemon for automatic remote deployment, making the network delay
configurable in a per message basis.

%termination detection + shutdown
%deployment with daemon
%graph represented as json python-graph removed, graphs changed, conf.py
%changed, gen_graphs changed


\section{How it works}


\subsection{Multiple iterations of bird launches}

In the previous labs we have already dealt with multiple bird launches, but each one
in a separate ``day'' (or epoch), i.e.\ the entire system state was
reseted after each bird launch. We now all multiple bird launches in the same
``round''. The last bird launch is indicated by a flag in that message, which
allows a leader to start executing termination detection and then query the pigs
statuses. We keep the system state throughout the entire
round, i.e. once a pig is hurt it remains so until the round finishes.

Instead of manually specifying the moment of every bird launch, we randomly
draw the time between consecutive launches from the exponential distribution
with rate $1.0/\textrm{bird\_falltime}$.

Some time before the bird lands a {\it bird\_notice} message
is sent from the bird process to the leaders, telling them where the bird will
land. The time when {\it bird\_notice} and {\it bird\_landed} messages are sent
is controlled by the parameters {\it bird\_seen} and {\it bird\_falltime},respectively.

\subsection{Monitoring the load on the server (extra credit)}

The primary leader has an extra job in this lab: he keeps track of the number of
birds launched and estimates the bird launch rate. The estimate is simply given by
the ratio between the number of birds launched so far and the time since its
initialization. If the estimated bird launch rate
is above a threshold (set in \verb|helper_threshold|), the primary leader sends a
message to a daemon (see next section), in order to start a helper.

\subsection{Replicating the coordinator (extra credit)}

A daemon was implemented to support dynamic node deployment. The daemon
(\verb|gen_node_daemon.py|) waits for a request from the primary leader to start a
helper. A helper (\verb|helper.py|) is a node with basic leader functionalities,
such as relaying the bird landed message and informing pigs that the bird
is approaching.
The helper executes until
the round finishes. At most one helper can be started per round. For simplicity,

The primary leader assigns responsibility for some of his pigs to the helper.
For simplicity, (1) pigs are initialized with the IP address of the helper (that
can be changed in \verb|conf.py| and (2) the helper
clones the connection pattern of the primary leader to the P2P
network, (3) we disable failures when the helper is running.
%Nodes in the P2P network only update their information about the local
%connections out of THE CRITICAL SECTION (XXX).

\begin{figure}[!h]
\centerline{
\subfloat[low load]{\includegraphics[width=2.5in]{figs/launches1.pdf}
\label{fig:launches1}}
\subfloat[high load]{\includegraphics[width=2.5in]{figs/launches5.pdf}
\label{fig:launches4}}
}
\centerline{
\subfloat[no helper]{\includegraphics[width=2.5in]{figs/helper1.pdf}
\label{fig:helper1}}
\subfloat[helper is started]{\includegraphics[width=2.5in]{figs/helper5.pdf}
\label{fig:helper4}}
}
\caption{Monitoring load and replicating coordinator.}
\label{fig:helper}
\end{figure}

\subsection{Two coordinators}

We decided to reuse our implementation of distributed leader election to elect
as coordinators the two nodes with the smallest ids (instead of hardcoding the
leaders). In our previous implementation of the leader election algorithm for Wireless
environments, each node returned to its parent (in the dissemination tree) only the
maximum id of its descendants. Although this suffices for electing one
coordinator, it lacks the ability to handle two. Rather than sending the two
smallest ids upstream in the dissemination tree, we decided to send all of them
because the leaders need to know the ids of all
pigs to decide who is in charge of each pig.

The node with smallest and
second smallest ids will be hereafter referred to as primary and secondary
leaders/coordinators. Once the primary leader is elected, it shuffles the list of pig ids and
then becomes responsible for the group of pigs in the first half of the list.
The primary leader then sends the partitions to the secondary
leader via a {\it node\_partition} message.
Although each leader is only responsible for one partition,
they are made aware of which pigs are in the complement
as they need to take over in case the other leader fails.

\subsection{Database server and consistency}

The database server is a separate process representing the ``town register'',
which is responsible for keeping track of
the pigs' statuses after each launch. Coordinators cache that information in a
local buffer. Once a coordinator queries the statuses of the pigs he is in
charge of, he writes (potential) changes to the local cache and sends an
``update'' message to the database. The database, in turn, after refreshing its
local data, sends an ``update'' message to the other coordinator. Note that
since coordinators take care of mutually exclusive sets of pigs, there is no
read-write or write-write conflict.

Regarding consistency, we emphasize that a coordinator will only query the
status of its pigs after a round finishes. At the time the responses
are received, the coordinator still do not have the most up-to-date information.
In fact, the information regarding the other set of pigs is possibly obsolete.
The coordinator then updates its local cache and the database. Recall that the database will push
information updated by one coordinator to the other. Therefore, after the
database gets updates and exchanges those between the two coordinators, all
replicas become consistent. To conclude, note that the intervals during which the
replicas are inconsistent do not affect the system operation.

Output example:
\begin{verbatim}
 31.050  1 cache {'1': '1', '3': '0', '5': '1'} // cache of leader 1
 31.053  [DB] update from 1: {'1': '1', '3': '0', '5': '1'}
 31.054  [DB] {'1': '1', '3': '0', '5': '1'}    // non-obsolete entries of DB
 31.056  0 cache {'1': '1', '3': '0', '5': '1'} // cache of leader 0
 32.057  0 cache {'1': '1', '0': '1', '3': '0', '2': '1', '5': '1', '4': '1'}
                                                // cache of 0 after update
 32.060  [DB] update from 0: {'0': '1', '2': '1', '4': '1'}
 32.062  [DB] {'1': '1', '0': '1', '3': '0', '2': '1', '5': '1', '4': '1'}
                                                // non-obsolete entries of DB
 32.063  1 cache {'1': '1', '0': '1', '3': '0', '2': '1', '5': '1', '4': '1'}
                                                // cache of 1 after update
\end{verbatim}

%(XXX) we have to be careful with the fact that a coordinator could fail
%after propagating the bird approaching and never update the pigs statuses in
%the database. (XXX)

\subsection{Coordinator failures: simulation and handling}

In this lab, we assume two coordinators cannot be down at the same time.
To ensure this, we use a distributed synchronization mechanism similar to
token-ring. Consider the existence of a single token in the system, which sits in one
of the coordinators at any given time. If a coordinator has the token, it
stays up during an exponentially distributed time with average {\it uptime}, after
which it shutdowns and stays down for an exponentially distributed time with average {\it
downtime}. After the coordinator resumes working, it will pass on the token to the
other coordinator.

We had a long discussion about possible consequences of
having a leader going down after certain points, such as after responding the {\it
ping} (``are you alive'' message) from the other leader. We concluded that
sticking to the decision -- about relaying the {\it bird\_approaching} to the
controlled pigs -- made at the first point in time is the best solution to avoid
that some pigs do not get the warning. A decision is made either when it sends a ping
(i.e., you are currently alive and will relay the warning properly) or when it
receives one (i.e., relaying if up or not relaying if down). Hence, if a node
goes to sleep immediately after deciding to relay the warning, it will still send it
after it comes up again (at-least-one semantics).

In the case a node is down and receives a ping, instead of not responding, we
opted for sending a NACK (see Section \ref{sec:tradeoffs} for the explanation). When a leader
receives a NACK, it adds the set of pigs taken care by the other leader to the
set of destinations of the {\it bird\_approaching} message.

\begin{figure}[!h]
\centerline{
\subfloat[One failure]{\includegraphics[width=2.0in]{figs/sleep_wake3.pdf}
\label{fig:sw3}}
\subfloat[Two failures]{\includegraphics[width=2.0in]{figs/sleep_wake1.pdf}
\label{fig:sw1}}
\subfloat[Three failures]{\includegraphics[width=2.0in]{figs/sleep_wake7.pdf}
\label{fig:sw7}}
}
\caption{Timeline showing up and downstates of coordinators.}
\label{fig:sw}
\end{figure}

%\paragraph{P2P network parameters.}
%
%The following parameters are read from a configuration file in the beginning of each
%experiment:
%\begin{itemize}
%  \setlength{\itemsep}{1pt}
%  \setlength{\parskip}{0pt}
%  \setlength{\parsep}{0pt}
%\item {\it topology}: topology of P2P network; how pigs are logically connected
%\item {\it pigs\_pos}: vector of initial positions of the pigs
%\item {\it pigs\_delay}: simulated delay of P2P communication
%\item {\it pigs\_rolltime}: how long it takes for the pig to roll one tile
%\item {\it cols\_pos}: vector containing the positions of the columns the pig is
%in charge of
%\item {\it cols\_height}: height of columns (assumed to be equal)
%\item {\it cols\_falltime}: how long it takes for a column to fall after
%\end{itemize}



%\section{Design trade-offs}
\input{design/tradeoffs}

%\section{Possible extensions}
\input{design/extensions}

%\section{How to run the program}
\input{design/how}


\end{document}
