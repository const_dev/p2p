#!/bin/bash

USAGE="usage: $0 [-r] [-v] [HELPER_HOST [DAEMON_DIRECTORY]]"

if `getopt -T >/dev/null 2>&1`; [ $? = 4 ]; then
    ARGS=`getopt -o "hrv" -n $0 -- "$@"`
else
    # to support the BSD getopt (default Mac OS X getopt)
    ARGS=`getopt "hrv" "$@"`
fi

if [ $? -ne 0 ]; then
    echo $USAGE
    exit 1
fi

eval set -- "$ARGS"

RANDOM_TEST=""
VERBOSE=""

while true; do
    case "$1" in
        -h)
            echo $USAGE
            exit 0
            ;;
        -r)
            RANDOM_TEST="-r"
            shift
            ;;
        -v)
            VERBOSE="-v"
            shift
            ;;
        --)
            shift
            break
            ;;
    esac
done

if [ ! -d py2 ]; then
    git clone git://github.com/pypa/virtualenv.git
    python2.7 virtualenv/virtualenv.py py2
    source py2/bin/activate
    pip install python-daemon
else
    source py2/bin/activate
fi

if [ $# == 1 ]; then
    HELPER_HOST=$1
    python world.py -H $HELPER_HOST $RANDOM_TEST $VERBOSE
elif [ $# == 2 ]; then
    HELPER_HOST=$1
    DAEMON_DIR=$2
    ssh $HELPER_HOST bash $DAEMON_DIR/daemon.sh start
    python world.py -H $HELPER_HOST $RANDOM_TEST $VERBOSE
    ssh $HELPER_HOST bash $DAEMON_DIR/daemon.sh stop
else
    python gen_node_daemon.py start
    python world.py $RANDOM_TEST $VERBOSE
    python gen_node_daemon.py stop
fi

