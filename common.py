import socket
import conf


# message headers
DB_UNICAST = '@'
WORLD_UNICAST = '!'
WORLD_BROADCAST = '#'
LOCAL_BROADCAST = '$'


DummyMessage = None


def merge_result(raw_results):
    return '#'.join(raw_results)


def null(dummy_arg):
    return DummyMessage


# Synchrouns functions with their corresponding reduce functions
#
# Most of these synchronous functions will output results, and the
# returned results will be aggregated through the specified reduce function.
# Otherwise, the synchronous function is just served as a blocked call
# returning a dummy message (with 'null' as the reduce function.)
reduce_func = {
    'status': merge_result,
    'elect_leader': merge_result,
    'query': merge_result,
    'report_info': merge_result,
    'transfer_info': merge_result,
    'term_detect': merge_result,
    'ping': merge_result,

    # make self_kill a sync_func so leader can send status
    # back and shutdown the world
    'self_kill': null,   # do nothing, just to block the function

    # blocked functions:
    'node_partition': null,
    'wait_status': null,
}


# Functions being processed without simulated network delay
zero_delay_funcs = ['bird_landed', 'pig_rolled', 'column_fell',
                    'self_kill', 'bird_notice']


# Just be explicit. This can be only: sync_funcs = reduce_func
sync_funcs = reduce_func.keys()


# blocking function!
def socket_send(addr, msg, wait_reply):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(addr)
    s.sendall(msg)
    result = None
    if wait_reply:
        result = s.recv(conf.SOCKET_BUFSIZE)
    s.close()
    return result


def send_msg(addr, msg_id, lc_time, recv_id_list, func, args=None, wait=None):
    # to send message from within the p2p network, set addr to the address
    # of the node itself
    if args is None or args == '':
        args = '-'

    if wait is None:
        wait = func in sync_funcs

    msg = '%s %s %s %s %s' % (msg_id, lc_time, recv_id_list, func, args)
    result = socket_send(addr, msg, wait)
    return result


def msg_sender_name(msg_id):
    sender_name = msg_id[1:].rsplit('-', 1)[0]
    return sender_name


def format_dict(d):
    return '{%s}' % ', '.join('%s:%s' % item for item in sorted(d.iteritems()))

