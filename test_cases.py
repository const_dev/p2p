from collections import namedtuple
import random
from itertools import product


TestCase = namedtuple('TestCase', ['n_tile',
                                   'topology',
                                   'bird_pos',
                                   'bird_seen',
                                   'bird_falltime',
                                   'pigs_pos',
                                   'pigs_delay',
                                   'pigs_rolltime',
                                   'cols_pos',
                                   'cols_height',
                                   'cols_falltime',
                                   'leader_avg_uptime',
                                   'leader_avg_downtime',
                                   'token_holding_time',
                                   'helper_threshold',
                                   ])


test_cases = [
        # scenario 1: |4|5|6|1|2|3|, B->|5|,|1|,|3|
        # no failure
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=50,
                 leader_avg_downtime=5,
                 token_holding_time=55,
                 helper_threshold=0.15,
                ),

        # 1 failure before 1st bird landed
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=2.5,
                 leader_avg_downtime=5,
                 token_holding_time=12.5,
                 helper_threshold=0.15,
                ),

        # 1 failure between 1st and 2nd
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=7.5,
                 leader_avg_downtime=5,
                 token_holding_time=17.5,
                 helper_threshold=0.15,
                ),

        # 1 failure between 2nd and 3rd
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=12.5,
                 leader_avg_downtime=5,
                 token_holding_time=17.5,
                 helper_threshold=0.15,
                ),

        # 1 failure after 3rd
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=17.5,
                 leader_avg_downtime=5,
                 token_holding_time=20,
                 helper_threshold=0.15,
                ),

        # 2 failures: before 1st and between 2nd and 3rd
        TestCase(n_tile=6,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 4, 5, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=2.5,
                 leader_avg_downtime=3,
                 token_holding_time=6.5,
                 helper_threshold=0.15,
                ),

        # scenario 2: |4|5|6|1|C|2|C|3|, B->|1|

        # no failure
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=10,
                 leader_avg_downtime=5,
                 token_holding_time=55,
                 helper_threshold=0.15,
                ),

        # 1 failure before 1st bird landed
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=2.5,
                 leader_avg_downtime=5,
                 token_holding_time=12.5,
                 helper_threshold=0.15,
                ),

        # 1 failure between 1st and 2nd
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=7.5,
                 leader_avg_downtime=5,
                 token_holding_time=17.5,
                 helper_threshold=0.15,
                ),

        # 1 failure between 2nd and 3rd
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=12.5,
                 leader_avg_downtime=5,
                 token_holding_time=17.5,
                 helper_threshold=0.15,
                ),

        # 1 failure after 3rd
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=17.5,
                 leader_avg_downtime=5,
                 token_holding_time=20,
                 helper_threshold=0.15,
                ),

        # 2 failures: before 1st and between 2nd and 3rd
        TestCase(n_tile=8,
                 topology='graph/graph1.json',
                 bird_pos=[1, 3, 5],
                 bird_seen=0.5,
                 bird_falltime=5,
                 pigs_pos=[3, 5, 7, 0, 1, 2],
                 pigs_delay=0.2,
                 pigs_rolltime=0.4,
                 cols_pos=[],
                 cols_height=1,
                 cols_falltime=1,
                 leader_avg_uptime=2.5,
                 leader_avg_downtime=3,
                 token_holding_time=6.5,
                 helper_threshold=0.15,
                ),
]


# TODO: to generate topologies according to number of pigs
def generate_random_test():
    """generator that produces random test cases"""

    runs = 1

    n_launch = 10
    n_pig = 6
    n_col = 6

    # template
    n_launch = 10
    n_tile = 20
    bird_seen = 0.5
    pigs_delay = 0.2
    pigs_rolltime = 0.4
    cols_height = 1
    cols_falltime = 1
    leader_avg_downtime = 3

    topology_candidates = [
            'graph/graph0.json',
            'graph/graph1.json',
            'graph/graph2.json',
    ]

    # test_types:
    # - helper_started: no downtime (9 cases)
    # - pigs_hurt:      no helper (27 cases)
    uptime_candidates = {
            'helper_started': [100],
            'pigs_hurt': [leader_avg_downtime * i for i in [2, 4, 6]],
    }

    bird_falltime_candidates = [5, 4, 3]

    helper_threshold_map = {
            'helper_started': 0.2,
            'pigs_hurt': 100,
    }

    for test_type in ['helper_started', 'pigs_hurt']:
        for topology, bird_falltime, leader_avg_uptime, run in product(
                topology_candidates,
                bird_falltime_candidates,
                uptime_candidates[test_type],
                xrange(runs)):

            bird_pos = random.sample(xrange(n_tile), n_launch)
            all_pos = random.sample(xrange(n_tile), n_pig + n_col)
            pigs_pos, cols_pos = all_pos[:n_pig], all_pos[n_pig:]
            token_holding_time = leader_avg_uptime + leader_avg_downtime * 2
            helper_threshold = helper_threshold_map[test_type]

            yield TestCase(n_tile,
                           topology,
                           bird_pos,
                           bird_seen,
                           bird_falltime,
                           pigs_pos,
                           pigs_delay,
                           pigs_rolltime,
                           cols_pos,
                           cols_height,
                           cols_falltime,
                           leader_avg_uptime,
                           leader_avg_downtime,
                           token_holding_time,
                           helper_threshold,
                          )

