DAEMON_PORT = 10101

# the maximum number of queued connections, parameter for socket.listen()
REQUEST_QUEUE_SIZE = 100

# buffer size for socket.recv()
SOCKET_BUFSIZE = 1024

# choice() always returns 1 if deterministic is set to True
deterministic = True

LOG_DIR = 'log'
LOG_FILE = 'world.log'
