from collections import defaultdict
from itertools import permutations
from random import sample
import json


def gen_bidirectional_ring(n_node):
    graph = {'node': [], 'edge': []}
    graph['node'] = nodes = [str(i + 1) for i in xrange(n_node)]

    for i in xrange(n_node):
        n1, n2 = nodes[i], nodes[(i + 1) % n_node]
        graph['edge'].append((n1, n2))
        graph['edge'].append((n2, n1))

    return graph


def gen_unidirectional_ring(n_node):
    graph = {'node': [], 'edge': []}
    graph['node'] = nodes = [str(i + 1) for i in xrange(n_node)]

    for i in xrange(n_node):
        graph['edge'].append((nodes[i], nodes[(i + 1) % n_node]))

    return graph


def gen_small_world(n_node, n_extra_edge):
    graph = gen_unidirectional_ring(n_node)

    edge_candidates = set(permutations(graph['node'], 2)) - set(graph['edge'])
    graph['edge'].extend(sample(edge_candidates, n_extra_edge))

    return graph


def read_graph(graph_file):

    with open(graph_file, 'r') as f:
        graph = json.load(f)

    nodes = graph['node']
    nodes.sort()

    edges = graph['edge']

    in_nodes = defaultdict(list)
    out_nodes = defaultdict(list)

    for from_node, to_node in edges:
        out_nodes[from_node].append(to_node)
        in_nodes[to_node].append(from_node)

    return nodes, in_nodes, out_nodes


if __name__ == '__main__':
    import pprint

    pprint.pprint(gen_bidirectional_ring(6))
    pprint.pprint(gen_unidirectional_ring(6))

    graph = gen_small_world(6, 3)
    pprint.pprint(graph)

    graph_file = 'test_graph.json'
    with open(graph_file, 'w') as f:
        json.dump(graph, f, indent=4)

    nodes, in_nodes, out_nodes = read_graph(graph_file)
    pprint.pprint(nodes)
    pprint.pprint(in_nodes)
    pprint.pprint(out_nodes)
