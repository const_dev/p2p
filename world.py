"""
Simulator of the distributed p2p network:

- Deploy pigs -- form a p2p network
- Simulate events following the configuration (test_cases.py)
"""

import argparse
import os
import logging
import SocketServer
import random
import socket
import threading
import time
from collections import defaultdict
from graph import read_graph
from db_server import DatabaseServer
from common import send_msg, msg_sender_name, WORLD_UNICAST, WORLD_BROADCAST
from node import P2P_Node
from test_cases import test_cases, generate_random_test
from conf import REQUEST_QUEUE_SIZE, SOCKET_BUFSIZE, LOG_DIR, LOG_FILE


USE_DAEMON = False

if USE_DAEMON:
    import xmlrpclib
    from conf import DAEMON_PORT


# delay between bird_notice and bird_landed message
BIRD_LANDING_DELAY = 1


PRINT_LINE_WIDTH = 50


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):

    # messages logged in debug level
    debug_msgs = ['#', 'sleep', 'wake', 'notice_rate', 'status_report',
                  'shutdown', 'world_shutdown']

    def handle(self):
        data = self.request.recv(SOCKET_BUFSIZE)
        pig, msg_type, args = data.split(None, 2)
        server = self.server

        print_msg = data
        if msg_type == 'bird_pos':
            self.request.sendall(str(server.bird_pos))
        elif msg_type == 'leaders':
            server.leaders = args
            server.leader_event.set()
            print_msg = 'elected leaders: %s' % ', '.join(args.split(','))
        elif msg_type == 'notice_rate':
            print_msg = '%s computes bird notice rate: %s' % (pig, args)
        elif msg_type == 'take_shelter':
            print_msg = '%s gets hurt and sends take_shelter to %s' % (pig,
                        parse_id_list(args))
        elif msg_type == 'sleep':
            print_msg = '%s goes to sleep' % pig
        elif msg_type == 'wake':
            print_msg = '%s wakes up' % pig
        elif msg_type == 'hurt_pig':
            print_msg = '%s is hit by rolling %s' % (pig, args)
        elif msg_type == 'hurt_bird':
            print_msg = '%s is hit by bird %s' % (pig, args)
        elif msg_type == 'hurt_col':
            print_msg = '%s is hit by column' % pig
        elif msg_type == 'hurt_move':
            new_position = int(args)
            print_msg = '%s rolls to position %d' % (pig, new_position)
        elif msg_type == 'move':
            new_position = int(args)
            print_msg = '%s escapes to position %d' % (pig, new_position)
        elif msg_type == '#':
            print_msg = parse_recv_msg(pig, args)
        elif msg_type == '$':
            print_msg = '%s %s' % (pig, args)
        elif msg_type == 'elected':
            print_msg = 'NEW LEADER ELECTED'
        elif msg_type == 'shutdown':
            print_msg = '%s shutdown' % pig
        elif msg_type == 'status_report':
            status_str = args
            print_msg = 'status %s' % status_str
            if server.status == '':
                server.status = status_str
            elif status_str != '':
                server.status += '#' + status_str

        self.request.close()

        with server.lock:
            if msg_type in self.debug_msgs:
                log = server.logger.debug
            else:
                log = server.logger.info
            log('%7.3f  %s', time.time() - server.start_time, print_msg)

        if msg_type == 'world_shutdown':
            server.shutdown()
            with server.lock:    # maybe redundant, but doesn't hurt.
                print_status(server.status, server.logger)


def parse_id_list(node_id_list):
    return ', '.join(sorted(node_id_list.split(',')))


def parse_recv_msg(pig_name, args):
    msg_id, sent_time, func, recv, args = args.split(None, 4)
    sender = msg_sender_name(msg_id)
    if sender == '':
        sender = 'W'
    if func == 'bird_approaching':
        position = args.split(None, 1)[0]
        return '%s knows bird will land at %s from %s' % (pig_name, position,
                                                          sender)
    if func == 'take_shelter':
        return '%s receives take_shelter from %s' % (pig_name, sender)
    if func == 'status':
        if recv != 'all':
            recv = parse_id_list(recv)
        return '%s receives status query for %s' % (pig_name, recv)
    return '[sender %s] (time %s) %s receives "%s %s"' % (
                sender, sent_time, pig_name, func, args)


def get_msg_id():
    msg_id = 0
    while True:
        yield msg_id
        msg_id += 1


def world_msg_id(msg_id):
    return '%sw-%d' % (WORLD_BROADCAST, msg_id)


def print_status(status, logger):
    logger.info('-' * PRINT_LINE_WIDTH)
    logger.info('status:')
    total_hurt = 0
    name_status = [s.split(':') for s in status.split('#')]
    for pig_name, pig_status in sorted(name_status):
        logger.info('%s: %s', pig_name, 'hurt' if pig_status == '1' else '-')
        if pig_status == '1':
            total_hurt += 1
    logger.info('# hurt: %d', total_hurt)


def deploy_local(pig_id):
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('', 0))
    pig_port = serversocket.getsockname()[1]
    pig_node = P2P_Node(pig_id, serversocket)

    return pig_port, pig_node.server_process


def get_host_addr():
    """
    Locate the public IP address of the current host
    """
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('8.8.8.8', 80))   # 8.8.8.8: Google's public DNS server
    host = s.getsockname()[0]
    s.close()
    return host


def send_bird_notice(addr, leaders, msg_id, bird_pos):
    bird_notice_arg = '%s %s' % (bird_pos, msg_id)
    send_msg(addr, msg_id, 0, leaders, 'bird_notice', bird_notice_arg)


def gen_world(conf, helper_host, logger):
    host_addr = get_host_addr()

    # Fix for "error: [Errno 32] Broken pipe"
    SocketServer.ThreadingTCPServer.request_queue_size = REQUEST_QUEUE_SIZE

    server = SocketServer.ThreadingTCPServer(('', 0),
                                             ThreadedTCPRequestHandler)
    server_addr = (host_addr, server.server_address[1])

    db = DatabaseServer()
    db_addr = db.addr
    db.world_addr = server_addr
    db.start()

    nodes, in_nodes, out_nodes = read_graph(conf.topology)

    logger.info('bird positions: %s', ', '.join(map(str, conf.bird_pos)))
    logger.info('column (height %d) positions: %s', conf.cols_height,
                ', '.join(map(str, conf.cols_pos)) if conf.cols_pos else
                '(none)')
    logger.info('pig positions: %s',
                ', '.join('%s:%s' % pig_pos
                          for pig_pos in zip(nodes, conf.pigs_pos)))
    logger.info('-' * PRINT_LINE_WIDTH)

    # information to pass to each pig:
    # - pig_id
    # - pig_name
    # - server_addr
    # - list of out_peer_id
    # - indegree
    # - position
    # - pigs_delay
    # - pigs_rolltime
    # - neighbors

    pig_addr = {}

    if USE_DAEMON:
        host = '127.0.0.1'
        proxy = xmlrpclib.ServerProxy('http://%s:%d/' % (host, DAEMON_PORT))
        for node in nodes:
            port = int(proxy.gen_node(node))
            pig_addr[node] = (host_addr, port)

    else:
        thread_list = []
        for node in nodes:
            port, thread = deploy_local(node)
            pig_addr[node] = (host_addr, port)
            thread_list.append(thread)

    pos_node_pair = zip(conf.pigs_pos, nodes)
    pos_node_pair.sort()
    position = {node: pos for pos, node in pos_node_pair}

    neighbors = defaultdict(list)
    for idx, (pos, node) in enumerate(pos_node_pair):
        if idx < len(pos_node_pair) - 1:
            neighbors[node].append(pos_node_pair[idx + 1][1])
        if idx > 0:
            neighbors[node].append(pos_node_pair[idx - 1][1])

    # compute owner of each stone
    stones = defaultdict(list)
    for col_pos in conf.cols_pos:
        nearest_node = min((abs(pig_pos - col_pos), node)
                           for pig_pos, node in pos_node_pair)[1]
        stones[nearest_node].append(col_pos)

    for node in nodes:
        info_str = ';'.join(map(str, [
                                '%s:%d' % pig_addr[node],
                                '%s:%d' % server_addr,
                                '%s:%d' % db_addr,
                                ','.join('%s:%d' % pig_addr[peer]
                                         for peer in sorted(out_nodes[node])),
                                len(in_nodes[node]),
                                ','.join(neighbors[node]),
                                position[node],
                                ','.join(str(pos) for pos in stones[node]),
                                conf.pigs_delay,
                                conf.pigs_rolltime,
                                conf.cols_height,
                                conf.cols_falltime,
                                helper_host,
                                conf.helper_threshold,
                                conf.leader_avg_uptime,
                                conf.leader_avg_downtime,
                                conf.token_holding_time,
                                len(conf.bird_pos),
                                ]))

        send_msg(pig_addr[node], WORLD_UNICAST, 0, node, 'init_info', info_str)

    time.sleep(1)

    server.bird_pos = conf.bird_pos
    server.cols_pos = conf.cols_pos
    server.cols_height = conf.cols_height
    server.cols_falltime = conf.cols_falltime
    server.logger = logger

    lock = threading.Lock()
    leader_event = threading.Event()
    server.lock = lock
    server.leader_event = leader_event
    server.status = ''

    server_thread = threading.Thread(target=server.serve_forever)
    server_thread.start()
    server.start_time = time.time()

    entry_node = pig_addr[nodes[-1]]

    #time.sleep(0.3)
    send_msg(entry_node, WORLD_UNICAST, 0, 1, 'init_election')

    leader_event.wait()
    msg_id = get_msg_id()

    leaders = server.leaders.split(',')
    db.leaders = {leader: pig_addr[leader] for leader in leaders}

    def log_info(msg):
        with lock:
            logger.info('%7.3f  %s', time.time() - server.start_time, msg)

    for bird, bird_pos in enumerate(conf.bird_pos):
        next_falltime = random.expovariate(1.0 / conf.bird_falltime)

        notice_delay = next_falltime - BIRD_LANDING_DELAY
        if notice_delay > 0:
            time.sleep(notice_delay)

        log_info('leaders start to notice the coming of bird %d' % bird)
        send_bird_notice(pig_addr[leaders[0]], 'leaders',
                         world_msg_id(msg_id.next()), bird_pos)

        time.sleep(BIRD_LANDING_DELAY)

        log_info('bird %d is landed at %s' % (bird, bird_pos))
        send_msg(entry_node, world_msg_id(msg_id.next()), 0,
                 'all', 'bird_landed', bird_pos)

    server_thread.join()
    #time.sleep(1)

    db.shutdown()
    db.thread.join()

    if not USE_DAEMON:
        for pig_thread in thread_list:
            pig_thread.join()


def run_test(test_cases, logger, helper_host, case_name=''):
    if case_name != '':
        case_name = case_name + ' '

    for idx, test_conf in enumerate(test_cases):
        logger.info('=' * PRINT_LINE_WIDTH)
        logger.info('%scase %d', case_name, idx + 1)
        logger.info('=' * PRINT_LINE_WIDTH)
        gen_world(test_conf, helper_host, logger)


def main():
    random.seed(0)

    parser = argparse.ArgumentParser()

    parser.add_argument('-H', dest='helper_host', default='localhost',
                        help='network address of helper launcher')

    parser.add_argument('-r', dest='random_test', action='store_true',
                        default=False, help='run randomly generated tests')

    parser.add_argument('-v', dest='verbose', action='store_true')

    args = parser.parse_args()

    helper_host = args.helper_host
    random_test = args.random_test

    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    if not os.path.isdir(LOG_DIR):
        os.mkdir(LOG_DIR, 0755)

    # output to both console and log file
    logging.basicConfig(level=log_level,
                        format='%(message)s',
                        filename=(LOG_DIR + '/' + LOG_FILE),
                        filemode='w')
    logger = logging.getLogger()
    logger.addHandler(logging.StreamHandler())

    run_test(test_cases, logger, helper_host)

    if random_test:
        run_test(generate_random_test(), logger, helper_host, 'random')


if __name__ == '__main__':
    main()
