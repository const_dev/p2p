import logging
import os
import socket
import SocketServer
from SimpleXMLRPCServer import SimpleXMLRPCServer, SimpleXMLRPCRequestHandler
from daemon import runner
from node import P2P_Node
from helper import Helper
from conf import DAEMON_PORT, LOG_DIR, REQUEST_QUEUE_SIZE


FULL_LOG_DIR = os.getcwd() + '/' + LOG_DIR


class ThreadedXMLRPCServer(SocketServer.ThreadingMixIn, SimpleXMLRPCServer):
    pass


class NodeDaemon():
    def __init__(self, logger):
        self.stdin_path = '/dev/null'
        self.stdout_path = '%s/out.txt' % FULL_LOG_DIR
        self.stderr_path = '%s/err.txt' % FULL_LOG_DIR
        self.pidfile_path = '%s/daemon.pid' % FULL_LOG_DIR
        self.pidfile_timeout = 5
        self.logger = logger

    def run(self):
        self.logger.info('start server')
        ThreadedXMLRPCServer.request_queue_size = REQUEST_QUEUE_SIZE
        server = ThreadedXMLRPCServer(('', DAEMON_PORT),
                                      SimpleXMLRPCRequestHandler,
                                      allow_none=True)
        self.server = server
        #server.register_introspection_functions()
        server.register_function(self.gen_node, 'gen_node')
        server.register_function(self.gen_helper, 'gen_helper')
        server.serve_forever()

    def gen_helper(self, node_id):
        serversocket, port = gen_serversocket()
        self.logger.info('generate helper with port %d' % port)
        Helper(node_id, serversocket)
        return port

    def gen_node(self, node_id):
        serversocket, port = gen_serversocket()
        self.logger.info('generate node %s with port %d' % (node_id, port))
        P2P_Node(node_id, serversocket)
        return port


def gen_serversocket():
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind(('', 0))
    port = serversocket.getsockname()[1]
    return serversocket, port


def main():
    if not os.path.isdir(FULL_LOG_DIR):
        os.mkdir(FULL_LOG_DIR, 0755)

    logger = logging.getLogger('DaemonLog')
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter(
                        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler = logging.FileHandler('%s/daemon.log' % FULL_LOG_DIR)
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    app = NodeDaemon(logger)
    daemon_runner = runner.DaemonRunner(app)

    # This ensures that the logger file handle does not get closed
    # during daemonization
    daemon_runner.daemon_context.files_preserve=[handler.stream]

    daemon_runner.do_action()


if __name__ == '__main__':
    main()

