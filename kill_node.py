import argparse
from common import send_msg, WORLD_UNICAST


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('node_id')
    args = parser.parse_args()
    send_msg(args.node_id, WORLD_UNICAST, 0, args.node_id, 'kill')


if __name__ == '__main__':
    main()
