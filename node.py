"""
Class definition of a pig in the pig-to-pig network
"""

from __future__ import division
import xmlrpclib
import socket
import errno
import multiprocessing
import threading
import time
import itertools
from collections import deque
import random
#import functools
import conf
from common import (reduce_func, sync_funcs, zero_delay_funcs,
                    DummyMessage, send_msg, socket_send, format_dict,
                    DB_UNICAST, WORLD_UNICAST,
                    WORLD_BROADCAST, LOCAL_BROADCAST)


if conf.deterministic:
    def choice(opts):
        return 1
else:
    from random import choice


"""
instructions to add new functions:
1. define a new function
   * the function should be able to parse arguments in the form of a
     single string.
2. register the function in register_funcs()
3. specify if a return message is expected in sync_funcs
4. for functions in sync_funcs, register corresponding reduce function
   in reduce_func
5. functions in sync_funcs should be decorated by timestamp_retval
"""

encoded_dummy_msg = '0 -'


def timestamp_retval(func):
    #@functools.wraps(func)   # forget about it. who cares?
    def wrapper(node, *args):
        current_time = node.lc_time
        result = func(node, *args)
        return node.timestamp_encode(result, current_time)
    return wrapper


def encode_retval(func):
    #@functools.wraps(func)
    def wrapper(node, *args):
        retval = func(node, *args)
        return encode_result(retval)
    return wrapper


class P2P_Node(object):

    def __init__(self, node_id, serversocket, world_addr=None):
        # Start listening before server thread started to avoid potentially
        # refusing connections from other clients
        self.serversocket = serversocket
        serversocket.listen(conf.REQUEST_QUEUE_SIZE)
        serversocket.settimeout(1)

        self.addr = None

        # NOTE: node_id cannot contain "-,;#$@ "
        self.node_id = str(node_id)

        self.world_addr = make_addr(world_addr)

        # NOTE: out_peers must not be empty
        self.out_peers = []   # list of addresses of network peers
        self.neighbors = []   # list of IDs of physical neighbors
        self.leader_exclude_list = deque()   # list of IDs to exclude
        self.is_leader = False
        self.is_helper = False
        self.is_primary_leader = False
        self.token_timer = None
        self.allow_send_token_event = None
        self.sleep_timer = None
        self.is_asleep = False
        self.bird_lock = None
        self.token_lock = None
        self.to_send_birdinfo = {}
        self.total_node = 0

        self.delay = 0
        self.func_delay = {}
        self.status_cache = None
        self.to_check_status = False

        self.is_hurt = False

        self.msg_count = 0
        self.bird_landed_count = 0

        #self.start_time = time.time()
        self.helper_threshold = 0

        self.leader_avg_uptime = 3600.0
        self.leader_avg_downtime = 1.0

        self.sender_count = {}
        self.register_funcs()
        self.log_msg = []

        #self.node_lock = threading.RLock(verbose=True)
        self.node_lock = threading.Lock()
        self.time_lock = threading.Lock()
        self.stone_lock = threading.Lock()
        self.status_lock = threading.Lock()
        self.term_lock = threading.Lock()

        # For termination detection
        self.term_request = False
        self.is_term_detector = False

        self.lc_time = 0
        self.event_log = []

        self.status_event = None
        self.shutdown = False
        self.to_shutdown = False
        self.server_process = multiprocessing.Process(target=self.start_server)
        self.server_process.daemon = True
        self.server_process.start()

    def init_info(self, args):
        """
        Initialize node configuration and parameters of the story setup

        Format
        ------
        "field1;field2;...;fieldn"

        Fields
        ------
        addr:
            "host:port"
            address of the node

        world_addr:
            "host:port"
            address of the world

        db_addr:
            "host:port"
            address of the database server

        peers:
            "addr_str1,addr_str2,...,addr_strn"
            addr_str: "host:port"
            list of addresses of peers in the p2p network

        indegree:
            int
            indegree of the node in the p2p network graph

        neighbors:
            "node_id1,node_id2,...,node_idn"
            node_id: int
            list of IDs of neighboring nodes in terms of physical position

        position:
            int
            physical position of the node

        stones:
            "pos1,pos2,...,posn"
            pos: int
            position of the nearby stone columns

        pigs_delay:
            float
            network delay of p2p communication

        pigs_rolltime:
            float
            dealy that pig rolls when hit by the bird of the column

        cols_height:
            int
            height of the column stone

        cols_falltime:
            float
            delay of column falling

        helper_host:
            hostname or IP address of the helper

        helper_threshold:
            float
            bird notice rate threshold to launch helper

        leader_avg_uptime:
            float
            leader up time

        leader_avg_downtime:
            float
            leader down time

        token_holding_time:
            float
            time for leader holding the token

        total_birds:
            int
            total number of birds that will land
        """

        (my_addr, world_addr, db_addr, peers, indegree,
         neighbors, position, stones, pigs_delay, pigs_rolltime,
         cols_height, cols_falltime, helper_host, helper_threshold,
         leader_avg_uptime, leader_avg_downtime,
         token_holding_time, total_birds) = args.split(';')

        self.addr = make_addr(my_addr)
        self.world_addr = make_addr(world_addr)
        self.db_addr = make_addr(db_addr)
        self.out_peers = [make_addr(addr) for addr in peers.split(',') if addr]
        self.indegree = int(indegree)
        self.neighbors = neighbors.split(',')
        self.position = int(position)
        self.stones = {int(pos) for pos in stones.split(',') if pos}

        self.delay = float(pigs_delay)
        self.pigs_rolltime = float(pigs_rolltime)
        self.cols_height = int(cols_height)
        self.cols_falltime = float(cols_falltime)
        self.helper_host = helper_host
        self.helper_threshold = float(helper_threshold)

        self.leader_avg_uptime = float(leader_avg_uptime)
        self.leader_avg_downtime = float(leader_avg_downtime)
        self.token_holding_time = float(token_holding_time)

        self.total_birds = int(total_birds)

        # release time clock
        self.time_lock.release()

    # Set control variable to break server loop
    def kill(self, dummy_arg=None):
        #print 'shutdown', self.node_id
        self.time_lock.release()
        self.shutdown = True

    @timestamp_retval
    @encode_retval
    def self_kill(self, dummy_arg):
        self.to_shutdown = True
        self.time_lock.release()
        # Just to make this call blocking
        return DummyMessage

    def start_server(self):
        serversocket = self.serversocket
        pool = []
        while not (self.shutdown and len(self.sender_count) == 0):
            try:
                clientsocket, addr = serversocket.accept()
                clientsocket.settimeout(None)
                #print 'connected from:', addr
                client_thread = threading.Thread(target=self.recv_msg,
                                                 args=(clientsocket,))
                client_thread.daemon = True
                pool.append(client_thread)
                client_thread.start()
            except socket.timeout:
                continue

        for thread in pool:
            thread.join()

        serversocket.close()

    def recv_msg(self, in_node_socket):
        """
        Read message from socket and process it
        """

        msg = in_node_socket.recv(conf.SOCKET_BUFSIZE)

        msg_id, sent_time, recv_id_list, func, args = decode_msg(msg)

        # close socket upon receiving request, just like asynchronous RPC
        if func not in sync_funcs:
            in_node_socket.close()

        ##print '-' * 30
        ##print self.node_id, msg_id, recv_id_list, func, args

        if msg_id == WORLD_UNICAST:
            #self.notify_world('[world] %s %s' % (func, args))

            self.time_lock.acquire()
            self.lc_time = max(sent_time, self.lc_time) + 1

            result_for_world = self.reg_func[func](args)

            if func in sync_funcs:
                in_node_socket.sendall(result_for_world)
            in_node_socket.close()

            return

        # msgs from world are:
        # (1) notifications of external events; or
        # (2) commands that need broadcasting
        msg_from_world = msg_id[0] == WORLD_BROADCAST

        if msg_from_world:
            # Hack the message ID so that from now on it is treated as
            # message from p2p nodes.
            # Setting of msg_from_world will prevent this from counting into
            # its incoming connections (to later check with indegree).
            msg_id = convert_msg(msg_id)

        elif is_from_db(msg_id):
            in_node_socket.close()
            with self.time_lock:
                self.db_func[func](args)
            return

        reply = self.process_func(msg_id, sent_time, recv_id_list, func, args,
                                  skip_delay=msg_from_world)

        # send replies thru the reverse path
        if func in sync_funcs:
            # FIXME: update timestamp according to reply messages
            reply_time = self.lc_time    # it's safe to read without locking

            if func not in zero_delay_funcs:
                time.sleep(self.delay)

            #print 'reply by %s [%s]' % (self.node_id, reply)
            in_node_socket.sendall(self.timestamp_encode(encode_result(reply),
                                                         reply_time))

            in_node_socket.close()

        if not msg_from_world:   # if msg_from_world skip counting in indegree
            # table cleanup
            with self.node_lock:
                self.sender_count[msg_id] -= 1

                # If message received from all in-edges, no longer
                # need to track it
                if self.sender_count[msg_id] == 0:
                    del self.sender_count[msg_id]

        if self.is_term_detector:
            with self.term_lock:
                to_detect_term = False

                # NOTE: don't need to check self.is_term_detector again
                # because the only place that self.is_term_detector is set
                # to False is the following block under "if to_detect_term:"
                if (len(self.sender_count) == 0
                        and (not self.term_request)
                        # Do what term_detect() does to avoid sending
                        # unnecessary term_detect message.
                        and (self.is_helper or (
                             self.bird_landed_count == self.total_birds
                             and (not self.is_leader or
                                  self.bird_notice_count == self.total_birds)))
                        and (not self.to_shutdown)):
                    to_detect_term = True
                    self.term_request = True

            if to_detect_term:
                term_status = self.process_func(self.gen_msg_id(),
                                                self.lc_time,
                                                'all', 'term_detect')

                self.is_term_detector = False

                if term_status:   # someone is still busy
                    # if not going to terminate, termination detector resigns
                    any_busy_node = term_status.split('#')[0]
                    self.process_func(self.gen_msg_id(), self.lc_time,
                                      any_busy_node, 'check_term')
                else:   # term_status is DummyMessage => all done
                    # leader gets status of the nodes in the network and
                    # terminates the network
                    self.process_func(self.gen_msg_id(), self.lc_time,
                                      'leaders', 'check_status')

                # Don't need to lock because self.is_term_detector is False
                # now. As a result, other threads of the current node will
                # never enter this block again.
                self.term_request = False

        with self.status_lock:
            # To make sure to_check_status block is executed by exactly
            # one thread of each node
            to_check_status = self.to_check_status
            self.to_check_status = False

        if to_check_status:
            # NOTE: this block will be run ONCE through out the entire game

            # primary leader reset group
            if self.original_group:
                self.group = self.original_group

            status = self.process_func(self.gen_msg_id(), self.lc_time,
                                       encode_recv_list(self.group), 'status')

            self.status_cache.update(parse_status(status))
            self.notify_world('$ cache %s' % format_dict(self.status_cache))
            self.notify_world('status_report %s' % status)

            # update db server
            self.db_send_msg('update %s' % status)
            self.db_event.wait()

            # wait until the other leader finish updating cache
            # otherwise deadlock will occur if db_update comes after self_kill
            if self.status_event is not None:
                self.status_event.set()

            if self.is_primary_leader:
                self.process_func(self.gen_msg_id(), self.lc_time,
                                  self.other_leader, 'wait_status')

                self.process_func(self.gen_msg_id(), self.lc_time,
                                  'all', 'self_kill')

                self.notify_world('world_shutdown -')
                self.world_addr = None

        if (not self.shutdown and len(self.sender_count) == 0
                              and self.to_shutdown):
            self.notify_world('shutdown -')
            self.shutdown = True

    def process_func(self, msg_id, sent_time, recv_id_list, func, args=None,
                     skip_delay=True):
        if not args:   # args is None or args == '' (eg, empty recipient)
            args = '-'

        # initialize reply as dummy msg, overwrite later if actual reply
        if func in sync_funcs:
            reply = DummyMessage

        msg_first_recv = False
        with self.node_lock:
            # Since topology might change when helpers are added in,
            # sender_count must be consistent between here and
            # the cleaning up procedure.
            #
            # sender_count counts down instead of counts up guarantees
            # that the indegree is fixed here within this critical session.
            if msg_id not in self.sender_count:
                self.sender_count[msg_id] = self.indegree
                msg_first_recv = True

            # For the consistency issue mentioned above,
            # we should make a copy of out_peers within the critical
            # session so later the message will only send to these peers
            # without the ones added in between.
            current_out_peers = self.out_peers[:]

        if msg_first_recv:
            if func in sync_funcs:
                result_buf = []
                result_lock = threading.Lock()
            else:
                result_buf = None
                result_lock = None

            # Simulate network delay at the receiver side when message
            # comes from other nodes.
            #
            # No delay when message is from the node itself. It is the
            # standard mechanism to send out message from within the
            # network.
            if not skip_delay:
                if func not in zero_delay_funcs:
                    time.sleep(self.delay)

            # start updating logical clock
            self.time_lock.acquire()
            self.lc_time = max(sent_time, self.lc_time) + 1

            # Propagate message thru all outgoing edges with threads
            peer_thread_list = []
            for peer_addr in current_out_peers:
                #print 'connect: %s -> %s' % (self.node_id, peer_addr)
                peer_thread = threading.Thread(target=self.send_msg_and_save,
                        args=(peer_addr, msg_id, recv_id_list, func, args,
                              result_buf, result_lock))
                peer_thread.start()
                peer_thread_list.append(peer_thread)

            # execute instruction if I'm a target node
            if (self.node_id in recv_list(recv_id_list)
                    or (recv_id_list == 'leaders' and self.is_leader)
                    or recv_id_list == 'all'):
                self.notify_world('# %s %s %s %s %s' %
                                (msg_id, sent_time, func, recv_id_list, args))
                #self.notify_world('$ %s %s %s %s %s' %
                #                (msg_id, sent_time, func, recv_id_list, args))
                result = self.reg_func[func](args)

                #print 'result', result

                # insert result into result_buf if func is in sync_funcs
                if func in sync_funcs:
                    self.update_result_buf(result_buf, result, result_lock)
            else:
                self.time_lock.release()

            for peer_thread in peer_thread_list:
                peer_thread.join()

            #print self.node_id, func, 'result buf', result_buf

            # Overwrite dummy reply if sync call
            if func in sync_funcs:
                reply = reduce_func[func]([decode_result(res)
                                           for res in result_buf
                                           if not_dummy_result(res)])

        if func in sync_funcs:
            return reply

    def timestamp_encode(self, result, lc_time=None):
        if lc_time is None:
            lc_time = self.lc_time
        return '%d %s' % (lc_time, result)

    def timestamp_decode(self, result):
        result_time, result = result.split(None, 1)
        result_time = int(result_time)

        # Update timestamp
        with self.time_lock:
            self.lc_time = max(result_time, self.lc_time) + 1

        return result

    def update_result_buf(self, result_buf, result, result_lock):
        result = self.timestamp_decode(result)

        with result_lock:
            result_buf.append(result)

    def send_msg_and_save(self, addr, msg_id, recv_id_list,
                          func, args, result_buf, result_lock):

        result = send_msg(addr, msg_id, self.lc_time, recv_id_list, func, args)

        if func in sync_funcs:
            self.update_result_buf(result_buf, result, result_lock)

    def db_send_msg(self, msg):
        msg = '%s %s' % (self.node_id, msg)
        socket_send(self.db_addr, msg, False)

    def notify_world(self, msg):
        msg = '%s %s' % (self.node_id, msg)
        if self.world_addr is None:
            print msg
        else:
            socket_send(self.world_addr, msg, False)

    def register_funcs(self):
        self.reg_func = {
            'bird_notice': self.bird_notice,
            'bird_approaching': self.bird_approaching,
            'status': self.status,
            'take_shelter': self.take_shelter,
            'bird_landed': self.bird_landed,
            'column_fell': self.column_fell,
            'pig_rolled': self.pig_rolled,
            'kill': self.kill,

            'init_info': self.init_info,
            'report_info': self.report_info,
            'transfer_info': self.transfer_info,

            'init_election': self.init_election,
            'update_leader': self.update_leader,
            'query': self.query,
            'elect_leader': self.elect_leader,
            'node_partition': self.node_partition,
            'take_token': self.take_token,
            'ping': self.ping,

            'term_detect': self.term_detect,
            'self_kill': self.self_kill,
            'check_term': self.check_term,
            'check_status': self.check_status,
            'wait_status': self.wait_status,
        }

        self.db_func = {
            'db_update': self.db_update,
        }

    def db_update(self, status):
        self.status_cache.update(parse_status(status))
        self.notify_world('$ cache %s' % format_dict(self.status_cache))
        self.db_event.set()

    def check_term(self, dummy_arg):
        self.is_term_detector = True
        self.time_lock.release()

    def leader_resign(self):
        self.time_lock.acquire()
        self.leader_exclude_list.append(self.node_id)
        self.lc_time += 1
        self.init_election()

    def get_id_list(self, n_leader=2):
        id_list = self.process_func(self.gen_msg_id(), self.lc_time, 'all',
                                    'elect_leader',
                                    list_to_arg(self.leader_exclude_list))

        return id_list.split('#')

    def init_election(self, args=None):
        # set leader to None: don't do any leader works such as
        # terminate detection from now on
        self.is_leader = False

        self.time_lock.release()

        id_list = self.get_id_list()
        id_list.sort()   # leaders are those with two smallest (string) id
        new_leaders = id_list[:2]
        self.process_func(self.gen_msg_id(), self.lc_time,
                          encode_recv_list(new_leaders),
                          'update_leader', list_to_arg(id_list))

    @timestamp_retval
    @encode_retval
    def query(self, args):
        self.time_lock.release()
        if args == 'leader':
            return self.leader
        return 'N/A'

    def update_leader(self, args):
        self.time_lock.release()

        id_list = arg_to_list(args)
        leader1, leader2 = id_list[:2]

        if self.node_id == leader1:
            self.is_leader = True
            self.has_token = True
            self.is_primary_leader = True
            # for computing bird notice rate
            self.start_time = time.time()
            self.helper_lock = threading.Lock()
            self.is_term_detector = True
            self.other_leader = leader2
            rest_nodes = id_list[2:]
            random.shuffle(rest_nodes)
            group1_len = len(rest_nodes) // 2
            group1 = rest_nodes[:group1_len] + [leader1]
            group2 = rest_nodes[group1_len:] + [leader2]
            partition = '%s;%s' % (','.join(group1), ','.join(group2))
            self.group = group1
            self.other_group = group2

            # make a "blocking" call to leader2
            self.process_func(self.gen_msg_id(), self.lc_time,
                              leader2,
                              'node_partition', partition)

            # notify world the two leaders
            self.notify_world('leaders %s,%s' % (leader1, leader2))

        elif self.node_id == leader2:
            self.is_leader = True
            self.status_event = threading.Event()
            self.has_token = False
            self.other_leader = leader1
        else:
            self.is_leader = False

        if self.is_leader:
            self.bird_notice_count = 0
            self.has_helper = False
            self.timers_stopped = False
            self.original_group = None
            self.status_cache = {}
            self.token_lock = threading.Lock()
            self.bird_lock = threading.Lock()
            self.to_send_birdinfo = {}
            self.allow_send_token_event = threading.Event()
            self.allow_send_token_event.set()   # disable barrier
            self.db_event = threading.Event()
            # start random sleeping timer
            self.is_sleep = False
            self.set_sleep_timer()

        if self.has_token:
            # exchange token
            self.set_token_timer()

    def toggle_sleep(self):
        with self.token_lock:
            if self.is_sleep or self.has_token:
                self.is_sleep = not self.is_sleep
                self.notify_world('sleep -' if self.is_sleep else
                                  'wake -')

        # stop reseting timer after 'sleep' -> 'wake'
        if self.is_sleep or not self.has_token:
            self.set_sleep_timer()

    def send_token(self):
        if self.timers_stopped:
            return

        with self.token_lock:
            self.allow_send_token_event.wait()
            self.has_token = False

            if self.is_sleep:
                self.notify_world('wake -')

            # wake up and then send token
            self.is_sleep = False

        self.notify_world('$ sends token')
        self.process_func(self.gen_msg_id(), self.lc_time,
                          self.other_leader, 'take_token')

    def take_token(self, dummy_arg):
        self.notify_world('$ gets token')
        with self.token_lock:
            #self.allow_send_token_event.set()
            self.has_token = True

        self.time_lock.release()
        self.set_token_timer()

    def set_token_timer(self):
        if not self.timers_stopped:
            self.token_timer = threading.Timer(self.token_holding_time,
                                               self.send_token)
            self.token_timer.start()

    def set_sleep_timer(self):
        if not self.timers_stopped:
            interval = self.leader_avg_downtime if self.is_sleep else \
                       self.leader_avg_uptime
            self.sleep_timer = threading.Timer(interval, self.toggle_sleep)
            self.sleep_timer.start()

    def stop_token_timer(self):
        self.token_timer.cancel()

    @timestamp_retval
    @encode_retval
    def node_partition(self, args):
        self.time_lock.release()
        # this is called only in leader2
        group1_str, group2_str = args.split(';')
        group1 = group1_str.split(',') if group1_str else []
        group2 = group2_str.split(',') if group2_str else []

        self.group = group2
        self.other_group = group1

        return DummyMessage

    @timestamp_retval
    @encode_retval
    def wait_status(self, dummy_arg):
        self.time_lock.release()
        self.status_event.wait()
        return DummyMessage

    def check_status(self, dummy_arg):
        self.time_lock.release()
        self.to_check_status = True

    @timestamp_retval
    @encode_retval
    def status(self, args):
        is_hurt = self.is_hurt
        self.time_lock.release()
        return '%s:%d' % (self.node_id, is_hurt)

    def parse_network_info(self):
        # parse network_info
        # format: "node_id;position;stone_position_list"
        nodes_info = [info.split(';') for info in self.network_info.split('#')]

        # pig -> pos
        pig_pos_map = {info[0]: int(info[1]) for info in nodes_info}

        cols_pos = set(itertools.chain.from_iterable(
                                        map(int, info[2].split(','))
                                        for info in nodes_info if info[2]))
        return pig_pos_map, cols_pos

    def estimate_bird_impact(self):
        pig_pos_map, cols_pos = self.parse_network_info()

        pigs_pos = set(pig_pos_map.values())

        # compute bird impact
        collisions = deque()    # queue of collisions yet-to-be-handled
        impacted_pig_pos = set()

        bird_pos = self.bird_position

        if bird_pos in pigs_pos:
            impacted_pig_pos.add(bird_pos)
            collisions.append(('pig', bird_pos))

        if bird_pos in cols_pos:
            collisions.append(('col', bird_pos))

        while len(collisions) > 0:
            agent, position = collisions.popleft()

            if agent == 'pig':
                # Pig in 'position' is hurt. It won't change anything after
                # rolling.
                if position in pigs_pos:
                    pigs_pos.remove(position)

                # Do exactly what's in get_hurt() and pig_rolled()
                # Simulate one possibility only
                position += choice([-1, 1])

                if position in pigs_pos:
                    impacted_pig_pos.add(position)
                    # that's it, pig don't roll again

                # Simulate check_column_fell()
                if position in cols_pos.copy():
                    collisions.append(('col', position))

            else:   # agent == 'col'
                # remove column because it won't fall again
                if position in cols_pos:
                    cols_pos.remove(position)

                # Simulate check_column_fell()
                lo, hi = self.col_fell_range(position)

                for pig_pos in pigs_pos:
                    if lo <= pig_pos <= hi:
                        impacted_pig_pos.add(pig_pos)
                        collisions.append(('pig', pig_pos))

                for col_pos in cols_pos.copy():
                    if lo <= col_pos <= hi:
                        collisions.append(('col', col_pos))

        impacted_pigs = [pig_id for pig_id, pos in pig_pos_map.iteritems()
                                if pos in impacted_pig_pos]

        return impacted_pigs

    @timestamp_retval
    @encode_retval
    def term_detect(self, args):
        # Set term_status to '' to indicate the current node is done
        # and ready to terminate
        term_status = self.node_id   # continue

        # Terminate when the pig has seen bird landed and has no pending jobs.
        # At this point, the only message in sender_count should be the
        # 'term_detect' message.
        if len(self.sender_count) == 1:   # no pending jobs
            if self.is_helper:
                # Helper does not need to check self.bird_landed_count
                # or self.bird_notice_count.
                # Other leaders will take care of them.
                term_status = ''
            elif self.bird_landed_count == self.total_birds:
                if not self.is_leader:
                    term_status = ''
                elif self.bird_notice_count == self.total_birds:
                    # Leaders should check self.bird_notice_count
                    term_status = ''

        self.time_lock.release()

        if self.is_leader and not self.timers_stopped:
            self.stop_timers()

        return term_status

    def stop_timers(self):
        self.is_sleep = False
        self.timers_stopped = True

        # NOTE: to check if timer has started before canceling it
        if self.token_timer is not None:
            self.token_timer.cancel()
        if self.sleep_timer is not None:
            self.sleep_timer.cancel()

    @timestamp_retval
    @encode_retval
    def report_info(self, args):
        """
        Report its position and the owned columns to the leader
        """
        my_info = '%s;%d;%s' % (self.node_id,
                                self.position,
                                ','.join(str(pos) for pos in self.stones))

        self.time_lock.release()
        return my_info

    @timestamp_retval
    @encode_retval
    def transfer_info(self, args):
        """
        Previous leader transfers the position and owned columns of all
        the pigs to the new leader
        """
        msg = self.network_info

        # clean up the network info because this node is no longer the leader
        self.network_info = ''

        self.time_lock.release()

        return msg

    @timestamp_retval
    @encode_retval
    def elect_leader(self, args):
        if is_empty(args):
            self.leader_exclude_list = deque()
        else:
            self.leader_exclude_list = deque(args.split(','))

        leader_exclude_list = self.leader_exclude_list
        self.time_lock.release()

        if self.node_id in leader_exclude_list:
            return DummyMessage
        else:
            return self.node_id

    @timestamp_retval
    @encode_retval
    def ping(self, bird_msg_id):
        """
        return True if I am going to send out bird approaching message
        """
        with self.bird_lock:
            if bird_msg_id not in self.to_send_birdinfo:
                self.to_send_birdinfo[bird_msg_id] = not self.is_sleep
            will_send = '1' if self.to_send_birdinfo[bird_msg_id] else '0'

        self.time_lock.release()
        return will_send

    def bird_notice(self, args):
        position, bird_msg_id = args.split()
        self.bird_position = int(position)
        self.bird_notice_count += 1

        self.time_lock.release()

        # if holding the token, disable passing token from now on
        with self.token_lock:
            self.allow_send_token_event.clear()

        recv_list = self.group

        ping_other_leader = False

        with self.bird_lock:
            if bird_msg_id not in self.to_send_birdinfo: # I am not pinged
                if self.is_sleep:
                    self.to_send_birdinfo[bird_msg_id] = False
                else:
                    self.to_send_birdinfo[bird_msg_id] = True
                    ping_other_leader = True

        if ping_other_leader:
            will_send = self.process_func(self.gen_msg_id(),
                                          self.lc_time,
                                          self.other_leader,
                                          'ping', bird_msg_id)

            if will_send == '0':
                recv_list = recv_list + self.other_group
                self.notify_world("$ sends approaching message for %s" %
                                                            self.other_leader)

        # if holding the token, enable passing token
        # NOTE: never use self.token_lock here, otherwise deadlock will happen
        self.allow_send_token_event.set()

        if self.to_send_birdinfo[bird_msg_id]:
            self.process_func(self.gen_msg_id(), self.lc_time,
                              encode_recv_list(recv_list),
                              'bird_approaching',
                              '%d %s' % (self.bird_position, bird_msg_id))

        # *Cannot* remove this entry because ping may read this in the future
        #del self.to_send_birdinfo[bird_msg_id]

        # Primary leader is allowed to create a single helper
        if self.is_primary_leader:
            elapsed_time = time.time() - self.start_time
            bird_launch_rate = self.bird_notice_count / elapsed_time

            self.notify_world('notice_rate %g' % bird_launch_rate)

            to_gen_helper = False
            if bird_launch_rate > self.helper_threshold:
                with self.helper_lock:
                    if not self.has_helper:
                        self.has_helper = True
                        to_gen_helper = True

            if to_gen_helper:
                self.gen_helper()

    def gen_helper(self):
        proxy = xmlrpclib.ServerProxy('http://%s:%d/' % (self.helper_host,
                                                         conf.DAEMON_PORT))
        helper_port = int(proxy.gen_helper('h' + self.node_id))
        helper_addr = (self.helper_host, helper_port)

        self.original_group = self.group[:]
        random.shuffle(self.group)
        group1_len = len(self.group) // 2
        group1 = self.group[:group1_len]
        group2 = self.group[group1_len:]

        info_str = ';'.join(map(str, ['%s:%d' % helper_addr,
                                      '%s:%d' % self.world_addr,
                                      '%s:%d' % self.db_addr,
                                      '%s:%d' % self.addr,
                                      1,
                                      ','.join(group2)]))

        # Consider this as "sudo"! Just to avoid broadcasting
        send_msg(helper_addr, WORLD_UNICAST, 0, 'helper', 'init_info',
                 info_str, wait=True)

        # dynamically change topology by adding: primary_leader <--> helper
        with self.node_lock:
            # modify topology
            self.out_peers.append(helper_addr)
            self.indegree += 1

            self.group = group1

            # leader has helper; not allow to go to sleep from now on.
            self.stop_timers()

        if self.bird_notice_count == self.total_birds:
            if not self.timers_stopped:
                self.stop_timers()

    def bird_approaching(self, args):
        if not self.is_hurt:
            self.bird_position = int(args.split(None, 1)[0])
            if self.bird_position == self.position:
                self.position += choice([-1, 1])

        self.time_lock.release()

    def take_shelter(self, args):
        if not self.is_hurt:
            self.position += choice([-1, 1])
            self.log('MV %d' % self.position)
            self.notify_world('move %s' % self.position)
        self.time_lock.release()

    def log(self, msg):
        """
        log events for the leader to compute pig status later

        NOTE: log() should be put in the critical section
        """
        self.log_msg.append('%d %s' % (self.lc_time, msg))

    def bird_landed(self, args):
        bird_position = int(args)
        self.bird_landed_count += 1

        check_col_thread = threading.Thread(target=self.check_column_fell,
                                            args=(bird_position,))
        check_col_thread.start()

        ##print '[*] bird landed at', bird_position
        self.log('BL %d' % bird_position)

        if self.position == bird_position:
            self.notify_world('hurt_bird %d' % self.bird_landed_count)
            self.get_hurt()    # NOTE: lock released in get_hurt()
        else:
            self.time_lock.release()

        check_col_thread.join()

    def column_fell(self, args):
        self.log('CF %s' % args)

        range_list = [map(int, rng.split(':')) for rng in args.split(',')]

        check_col_thread = threading.Thread(target=self.check_column_fell,
                                            args=(range_list,))
        check_col_thread.start()

        if (not self.is_hurt) and any(min_pos <= self.position <= max_pos
                                      for min_pos, max_pos in range_list):
            self.notify_world('hurt_col -')
            self.get_hurt()    # NOTE: lock released in get_hurt()
        else:
            self.time_lock.release()

        check_col_thread.join()

    # FIXME: can check_column_fell run outside of critical section?
    def check_column_fell(self, range_list):
        if isinstance(range_list, int):
            range_list = [(range_list, range_list)]

        remove_pos = []
        impact_range_list = []
        for col_pos in self.stones:
            if any(min_pos <= col_pos <= max_pos
                    for min_pos, max_pos in range_list):
                remove_pos.append(col_pos)
                impact_range_list.append(self.col_fell_range(col_pos))

        if not remove_pos:
            return

        with self.stone_lock:
            for pos in remove_pos:
                # It's possible that multiple columns fall on the same position
                if pos in self.stones:
                    self.stones.remove(pos)

        # merge impact_range_list to make it more compact if possible
        impact_range_list.sort()
        cur_min, cur_max = impact_range_list[0]
        compact_range_list = []
        for pos_min, pos_max in impact_range_list:
            if pos_min <= cur_max + 1:
                cur_max = pos_max
            else:
                compact_range_list.append((cur_min, cur_max))
                cur_min, cur_max = pos_min, pos_max
        compact_range_list.append((cur_min, cur_max))

        time.sleep(self.cols_falltime)
        self.process_func(self.gen_msg_id(), self.lc_time,
                          'all', 'column_fell',
                          ','.join(':'.join(map(str, rng))
                                   for rng in compact_range_list))

    def col_fell_range(self, col_pos):
        # compute the impacted range of the fell column
        if choice([0, 1]):
            lo, hi = col_pos + 1, col_pos + self.cols_height
        else:
            lo, hi = col_pos - self.cols_height, col_pos - 1
        return lo, hi

    def pig_rolled(self, args):
        pig_position, rolling_pig = args.split(None, 1)
        pig_position = int(pig_position)
        self.log('PR %d' % pig_position)

        if (not self.is_hurt) and (self.position == pig_position):
            self.is_hurt = True
            self.notify_world('hurt_pig %s' % rolling_pig)
            # that's it! no more rolling.

        # FIXME: Is it OK to unlock before checking column fell?
        #        Is that possible that things happen in between?
        self.time_lock.release()

        # Check if pig rolls on to a stone column
        self.check_column_fell(pig_position)

    def get_hurt(self):
        self.is_hurt = True
        self.time_lock.release()

        #self.notify_world('take_shelter %s' % encode_recv_list(self.neighbors))
        #self.process_func(self.gen_msg_id(), self.lc_time,
        #                  encode_recv_list(self.neighbors), 'take_shelter')

        time.sleep(self.pigs_rolltime)

        with self.time_lock:
            self.position += choice([-1, 1])
            #self.log('move %d' % self.position)
            move_time = self.lc_time

        self.notify_world('hurt_move %s' % self.position)

        # Broadcast pig_rolled message
        self.process_func(self.gen_msg_id(), move_time, 'all', 'pig_rolled',
                          '%d %s' % (self.position, self.node_id))

    def gen_msg_id(self):
        # prefix msg_id with $ in case self.node_id starts with #
        msg_id = '%s%s-%s' % (LOCAL_BROADCAST, self.node_id, self.msg_count)
        self.msg_count += 1
        return msg_id


def make_addr(node_id):
    try:
        host, port = node_id.split(':')
        return host, int(port)
    except:
        return None


def decode_msg(msg):
    msg_id, lc_time, recv_id_list, func, args = msg.split(None, 4)
    return msg_id, int(lc_time), recv_id_list, func, args


def encode_result(result):
    if not result:    # result == '' or result is DummyMessage
        return encoded_dummy_msg
    return '1 ' + result


def decode_result(result):
    result_type, raw_result = result.split(None, 1)
    return raw_result


def not_dummy_result(result):
    return result != encoded_dummy_msg


def is_empty(args):
    return args == '-'


def max_num(node_id_list):
    if len(node_id_list) == 0:
        return ''
    return str(max(int(node_id) for node_id in node_id_list))


def is_from_db(msg):
    return msg[0] == DB_UNICAST


def convert_msg(msg):
    return LOCAL_BROADCAST + msg[1:]


def encode_recv_list(neighbors):
    return list_to_arg(neighbors) if neighbors else '-'


def recv_list(recv_node_ids):
    return arg_to_list(recv_node_ids) if recv_node_ids != '-' else []


def list_to_arg(node_ids):
    return ','.join(node_ids)


def arg_to_list(arg):
    return arg.split(',')


def parse_status(status_str):
    status = dict(st_str.split(':') for st_str in status_str.split('#')
                                    if st_str)
    return status


def check_socket_connection(addr):
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect(addr)
        s.close()
    except socket.error as e:
        if e.errno == errno.ECONNREFUSED:
            return None
        else:
            raise
    return addr

