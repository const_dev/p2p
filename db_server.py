import threading
import SocketServer
import conf
from common import send_msg, socket_send, DB_UNICAST, format_dict


class ThreadedTCPRequestHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        db_obj = self.server.db_obj

        data = self.request.recv(conf.SOCKET_BUFSIZE)
        sender, func, args = data.split(None, 2)

        # handle protocol here
        if func == 'update':
            self.request.close()
            status = dict(st_str.split(':') for st_str in args.split('#'))
            db_obj.send_log('$ update from %s: %s' % (sender,
                                                      format_dict(status)))
            db_obj.update(status)
            db_obj.send_log('$ %s' % format_dict(db_obj.status_db))

            for leader, addr in db_obj.leaders.iteritems():
                if leader != sender:
                    send_msg(addr, DB_UNICAST, 0, leader, 'db_update', args)


class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass


class DatabaseServer(object):
    def __init__(self):
        self.status_db = {}
        self.db_lock = threading.Lock()
        server = ThreadedTCPServer(('', 0), ThreadedTCPRequestHandler)
        self.server = server
        host, port = self.addr = server.server_address
        server.db_obj = self
        self.world_addr = None    # will be set from the world

        server_thread = threading.Thread(target=server.serve_forever)
        server_thread.daemon = True
        self.thread = server_thread

    def start(self):
        self.thread.start()

    def update(self, status):
        with self.db_lock:
            self.status_db.update(status)

    def send_log(self, msg):
        socket_send(self.world_addr, '[DB] ' + msg, False)

    def shutdown(self):
        self.server.shutdown()


