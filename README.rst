Angry Birds: A Peer-to-Peer Network Simulator
=============================================

Description
-----------

A peer-to-peer networking engine that demonstrates several distributed
algorithms under the theme of Angry Birds.

Detailed descriptions can be found at:

- http://lass.cs.umass.edu/~shenoy/courses/spring13/labs/lab1
- http://lass.cs.umass.edu/~shenoy/courses/spring13/labs/lab2
- http://lass.cs.umass.edu/~shenoy/courses/spring13/labs/lab3


Requirement
-----------

- `Python 2.7 <http://www.python.org/download/releases/2.7.4/>`_
- `python-daemon <http://pypi.python.org/pypi/python-daemon/>`_:
  A library implements Unix daemon processes


Features
--------

- Support arbitrary strongly connected network topology (directed graph)
- Basic synchronous/asynchronous RPC framework
- Leader election
- Logical clock synchronization
- Recursive events handling (e.g., column falling, pig rolling) until stable
- Fault tolerance
- Guarantee at least one leader is functioning
- Cache consistency between database server and leader nodes
- Load balancing among leaders
- Dynamic local/remote helper launching
- Dynamic network topology modification
- Using daemon to facilitate launch of helper nodes through RPC
- Termination detection


Usage
-----

The bash script ``easy_run.sh`` is an easy-to-use simulator interface::

   easy_run.sh [-h] [-r] [-v] [REMOTE_HOST [DAEMON_DIRECTORY]]

- ``-h``: show this usage and exit
- ``-r``: run random test cases
- ``-v``: show more detailed information
- ``REMOTE_HOST``: the remote address where the helper will be launched
- ``DAEMON_DIRECTORY``: the absolute path where the source code of this
  system is placed on the host specified in ``REMOTE_HOST``

Detailed information will be described later.

Single Host Simulation
----------------------

The easiest way to run the program is to use the following command::

   $ ./easy_run.sh

It will install all of the required packages automatically and activate the
working environment to run the program.

You can also install required packages manually and run the program by::

   $ python world.py


Launching Remote Helper
-----------------------

The easiest way to test launching helper on remote machine is to
first deploy ssh key to remote machine and then run ``easy_run.sh``
using the following two-argument syntax::

    $ ./easy_run.sh REMOTE_HOST DAEMON_DIRECTORY

where ``REMOTE_HOST`` is the host address that can be accessed through ssh.
Make sure that there is a copy of the source code in the directory
``DAEMON_DIRECTORY`` specified by its full path.

To manually start the daemon on the remote machine, just enter the source
directory on the remote host and fire the following command::

    $ python gen_node_daemon.py start

Notice that a daemon log file ``log/daemon.log`` will be generated
to keep track of helper launching history.
The standard output and standard error of the helper process
will be redirected to ``log/out.txt`` and ``log/err.txt`` respectively.

Once the daemon is ready, the simulator can then be run on the local machine::

    $ python world.py -H REMOTE_HOST

or alternatively using ``easy_run.sh`` with the one-argument syntax::

    $ ./easy_run.sh REMOTE_HOST

where ``REMOTE_HOST`` is the same host specified previously with the
started daemon.

After the simulator finishes, the remote daemon can be shutdown by
the command firing on the remote host::

    $ python gen_node_daemon.py stop


Simulator Log
-------------

A log file with identical contents as shown on the console is
placed in ``log/world.log`` each time the simulator is started.
Unlike the daemon log ``daemon.log`` that keeps all the history,
results from previous simulation will be discarded and
replaced by new contents.
