import argparse
import socket
from common import DummyMessage
from node import P2P_Node, encode_recv_list, make_addr


class Helper(P2P_Node):

    def init_info(self, args):
        disable_funcs = [
                'check_column_fell',
                'column_fell',
                'pig_rolled',
                'take_shelter',
                'status',
                'bird_approaching',
                'bird_landed',
                'check_status',
                ]

        with self.node_lock:
            for func in self.reg_func:
                if func in disable_funcs:
                    # disable unrelated functions
                    self.reg_func[func] = self.dummy_func

            self.is_leader = True
            self.is_helper = True
            self.is_primary_leader = False
            self.timers_stopped = True

            (my_addr, world_addr, db_addr,
                      peers, indegree, group) = args.split(';')

            self.addr = make_addr(my_addr)
            self.world_addr = make_addr(world_addr)
            self.db_addr = make_addr(db_addr)
            self.out_peers = [make_addr(addr) for addr in peers.split(',')
                                              if addr]
            self.indegree = int(indegree)
            self.group = group.split(',') if group else []

        self.time_lock.release()
        self.notify_world('$ becomes helper')

    def bird_notice(self, args):
        self.time_lock.release()
        position, bird_msg_id = args.split()
        #self.notify_world('$ helper send bird_approaching of %s to %s' % (
        #                  bird_msg_id, self.group))
        self.process_func(self.gen_msg_id(), self.lc_time,
                          encode_recv_list(self.group),
                          'bird_approaching',
                          '%s %s' % (position, bird_msg_id))

    def dummy_func(self, dummy_arg):
        self.time_lock.release()
        return DummyMessage


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', dest='node_id', required=True)
    args = parser.parse_args()

    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(('', 10111))
    node = Helper(args.node_id, serversocket)

    node.server_process.join()


if __name__ == '__main__':
    main()
